import time
import sys
import glob
import serial
import logging
import pylibftdi as ftdi

logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)


class LuxometerDeviceFTDI:
    def __init__(self):
        super().__init__()
        self.__dev = None

    @property
    def name(self):
        return f"{self.__class__.__name__}"

    @property
    def online(self):
        dev = None
        try:
            dev = ftdi.Device()
        except ftdi._base.FtdiError as e:
            logging.error("FtdiError. Turn on debug mode to see it")
            logging.debug(e)
            return False
        else:
            dev.close()
            return True

    def connect(self):
        self.__dev = ftdi.Device()
        self.__dev.baudrate = 9600
        if self.__dev is None:
            logging.error("FTDI device not connected")
        else:
            logging.error(f"FTDI device connected {self.__dev}")

    def readFromDevice(self):
        if self.__dev is None:
            logging.debug("MenosGrandes ftdi device is none")
            return 0
        if self.__dev._opened is False:
            logging.debug("Device not opened")
            return 0
        ret = -1
        # self.__dev.write(b"b") MENOSGRANDES TEMPORARY!
        logging.debug("MenosGrandes readFromDevice")
        ret = int.from_bytes(self.__dev.read(2), "big")
        # read_line = self.__dev.().strip()
        logging.debug(f"MenosGrandes readFromDevice = {ret}")
        return 23

    def disconnect(self):
        if self.__dev:
            self.__dev.close()


class LuxometerDeviceSerial:
    def __init__(self):
        self.__dev = None
        port = "/dev/ttyACM0"
        baudrate = 9600
        self.__dev = serial.serial_for_url(
            port, baudrate, rtscts=False, dsrdtr=False, do_not_open=True
        )
        self.__dev.timeout = 1
        self.__dev.dtr = 0
        self.__dev.rts = 0
        self.__dev.open()
        self.handshake = False

    @property
    def online(self):
        logging.debug("Call for online")
        if self.handshake is True:
            if self.__dev is not None:
                logging.debug("Check if not none")
                if self.__dev.is_open:
                    logging.debug("Port is open")
                    return True
        else:
            logging.debug("doHandshake")
            return self.doHandshake()

    def readFromDevice(self) -> float:
        if self.__dev is not None:
            if self.__dev.is_open:
                size = self.__dev.read(1)
                logging.debug(f"Readed size is:{size}")
                size_2 = int.from_bytes(size,"big")
                message = self.__dev.read((size_2));
                decoded = message.decode('UTF-8')
                logging.debug(f"Message is {decoded}")
                #logging.debug(f"{repr(mesage)}")
        return 0

    def connect(self):
        if self.__dev is not None:
            if self.__dev.is_open is False:
                self.doHandshake()

    def decodeCommand(self, msg):
        return f"<{bytearray.fromhex(hex(msg)[2:]).decode()}>".encode()

    def doHandshake(self):
        logging.debug(" doHandshake")
        return self.waitForDescriptor(9)

    def disconnect(self):
        logging.debug(" disconnect in TSL2591")
        if self.__dev is not None:
            self.__dev.close()

    def waitForDescriptor(self, descriptor):
        logging.debug("Wait for descriptor")
        sended = False
        sendOk = False
        while 1:
            size = self.__dev.read(1);
            logging.debug(f"{size}")
            mesage = self.__dev.read(size);
            logging.debug(f"{mesage}")
'''
def waitForDescriptor():
    port = "/dev/ttyACM1"
    baudrate = 9600
    __dev = serial.serial_for_url(
        port, baudrate, rtscts=False, dsrdtr=False, do_not_open=True
    )
    __dev.timeout = 1
    logging.debug(f"open")
    __dev.dtr = 0
    __dev.rts = 0
    __dev.open()
    sended = False
    sendOk = False
    while 1:
        while sendOk is False:
            __dev.write("<ok>".encode())
            logging.debug(f"send <ok> = {time.time()}")
            read_line = __dev.readline().strip()
            logging.debug(f"read_line = {read_line}")
            if read_line == b"r_ok":
                sendOk = True
            time.sleep(0.1)

        if sended is False:
            __dev.write("<ds>".encode())
            logging.debug(f"send <ds> = {time.time()}")
            sended = True

        r = 0
        read_line = __dev.readline().strip()
        logging.debug(f"receive= {time.time()}")
        logging.debug(f"read_line = {read_line}")
        try:
            r = int(read_line)
        except ValueError as e:
            pass

        if r == 9:
            logging.debug(f"log descritpro match!")
            while 1:
                __dev.write("<rl>".encode())
                read_line = __dev.readline().strip()
                logging.debug(f"log {read_line}")

                '''

# waitForDescriptor()
a = LuxometerDeviceSerial()
a.connect()
while True:
    logging.debug(a.readFromDevice())
    time.sleep(0.1)
