"""
(*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2021 Pupil Labs

Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""

from plugin import System_Plugin_Base
import zmq_tools
import logging

logger = logging.getLogger()
logger.handlers = []
logger.setLevel(logging.DEBUG)
# create logger for the context of this function
logger = logging.getLogger(__name__)

ch = logging.StreamHandler()
ch.setFormatter(
    logging.Formatter("%(processName)s - [%(levelname)s] %(name)s: %(message)s")
)
logger.addHandler(ch)


from launchables.luxometer.luxometer_notifications import (
    LuxometerNotification,
)


class Pupil_Data_Relay(System_Plugin_Base):
    """"""

    def __init__(self, g_pool):
        super().__init__(g_pool)
        self.order = 0.01
        self.gaze_pub = zmq_tools.Msg_Streamer(
            self.g_pool.zmq_ctx, self.g_pool.ipc_pub_url
        )
        self.pupil_sub = zmq_tools.Msg_Receiver(
            self.g_pool.zmq_ctx,
            self.g_pool.ipc_sub_url,
            topics=("pupil", LuxometerNotification.LUXOMETER_FRAME.value),
        )
        self.lux = 0
        self.luxTimestamp = 0

    # MenosGrandes TODO Gdzie to jest wywyolywane?
    def recent_events(self, events):
        recent_pupil_data = []
        recent_gaze_data = []
        while self.pupil_sub.new_data:
            topic, pupil_datum = self.pupil_sub.recv()
            if topic == LuxometerNotification.LUXOMETER_FRAME.value:
                logger.debug("MenosGrandes LuxFrame")
                self.lux = pupil_datum.get("luxValue", 0)
                self.luxTimestamp = pupil_datum.get("luxTimestamp", 0)
            else:
                recent_pupil_data.append(pupil_datum)

                gazer = (
                    self.g_pool.active_gaze_mapping_plugin
                )  # MenosGrandes set only after calibration!
                if gazer is None:
                    continue
                for gaze_datum in gazer.map_pupil_to_gaze([pupil_datum]):
                    pupil_datum["lux"] = self.lux
                    pupil_datum["lux_timestamp"] = self.luxTimestamp
                    self.gaze_pub.send(gaze_datum)
                    recent_gaze_data.append(gaze_datum)

        events["pupil"] = recent_pupil_data
        events["gaze"] = recent_gaze_data
