"""

 (*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2019 Pupil Labs
##
Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""
#
# # logging
# import logging
# import sched
# import time
#
# import numpy as np
#
# import usb.core
#
# ##
# logger = logging.getLogger(__name__)
# ##
# # MG UNUSED!!!!
# ##
# ##
#
#
# class LuxometerReader:
#     def connect(self):
#         self.usb_device = usb.core.find(idVendor=0x1A86, idProduct=0x7523)
#         print(self.usb_device)
#         if self.usb_device is None:
#             logger.error("Luxometer not found!")
#             return
#         else:
#             logger.info("Luxometer found!")
#         #
#         reattach = False
#         if self.usb_device.is_kernel_driver_active(0):
#             reattach = True
#             self.usb_device.detach_kernel_driver(0)
#         #
#         self.usb_device.set_configuration()
#         cfg = self.usb_device.get_active_configuration()
#         #
#         interface_number = cfg[(0, 0)].bInterfaceNumber
#         alternate_settting = usb.control.get_interface(
#             self.usb_device, interface_number
#         )
#         intf = usb.util.find_descriptor(
#             cfg, bInterfaceNumber=interface_number, bAlternateSetting=alternate_settting
#         )
#         #
#         ep = usb.util.find_descriptor(
#             intf,
#             custom_match=lambda e: usb.util.endpoint_direction(e.bEndpointAddress)
#             == usb.util.ENDPOINT_OUT,
#         )
#         lux = self.readFromDevice()
#         logger.error("Lux = {}".format(lux))
#         """
#        switch to onRemove? Or something like that function
#        """
#         # This is needed to release interface, otherwise attach_kernel_driver fails
#         # due to "Resource busy"
#         usb.util.dispose_resources(self.usb_device)
#         #
#         # It may raise USBError if there's e.g. no kernel driver loaded at all
#         if reattach:
#             self.usb_device.attach_kernel_driver(0)
#
#     #
#     ##
#     def readFromDevice(self):
#         data = self.usb_device.read(0x2, 100)
#         print(data)
#
#
# a = LuxometerReader()
# a.connect()
