from uuid import uuid4
from abc import ABC, abstractmethod


class LuxometerDeviceIf(ABC):
    def __init__(self):
        self.__id = uuid4()

    @abstractmethod
    def readFromDevice(self):
        pass

    @property
    @abstractmethod
    def online(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    def id(self):
        return self.__id

    @abstractmethod
    def disconnect(self):
        pass

    @property
    @abstractmethod
    def connected(self):
        pass

    @abstractmethod
    def connect(self):
        pass
