"""
Implementation of TSL2591 conectivity. Throught Arduino  UNO
"""
import logging
import serial
from .luxometer_device_if import LuxometerDeviceIf
from .descriptors import descriptors, commands, timing
from .exMessageHandler import EXMessageHandler, toInt, fromInt
from .exProtocol_pb2 import _EXMESSAGE, EXMessage, FetchLuxRequest, Valid

logger = logging.getLogger(__name__)


class WrongImplementationError(Exception):
    pass


class LuxometerDeviceSerial(LuxometerDeviceIf):
    def __init__(self, _port):
        super().__init__()
        self.__dev = None
        self.port = _port
        self.handshaked = True
        self.__exMessageHandler = EXMessageHandler()
        self.__ishandshaked = False
        self.startDevice()

    def startDevice(self):
        self.__dev = None
        baudrate = 9600  # MenosGrandes shoyld be in  descriptors file
        self.__dev = serial.serial_for_url(
            self.port, baudrate, rtscts=False, dsrdtr=False, do_not_open=True
        )
        self.__dev.timeout = timing.DEFAULT_DELAY_MS / 1000  # MenosGrandes CHECK THIS!
        self.__dev.dtr = 0
        self.__dev.rts = 0
        try:
            self.__dev.open()
        except:
            logger.error(f"Could not open {self.port}")
            return False
        return True

    @property
    def connected(self):
        return self.__ishandshaked

    @property
    def name(self):
        return f"{self.__class__.__name__}"

    @property
    def online(self):
        if self.__dev is not None:
            return self.__dev.is_open

    def handshake(self):
        if self.__dev is not None:
            if self.__dev.is_open:
                size = self.__dev.read(1)
                payload = self.__dev.read(toInt(size))  # and then the payload
                response = self.__exMessageHandler.handleMessage(payload)
                if response is not None:
                    # send by Serial?
                    self.__dev.write(
                        fromInt(response.ByteSize())
                    )  # first goes size of message
                    self.__dev.write(response.SerializeToString())
                    # second goest the message payload
                    self.__ishandshaked = True
                    return True

    def readFromDevice(self) -> float:
        # MenosGrandes send request and wait for response!
        fetchLuxReq = EXMessage()
        fetchLuxReq.fetchLuxReq._v = True
        self.__dev.write(fromInt(fetchLuxReq.ByteSize()))
        self.__dev.write(fetchLuxReq.SerializeToString())

        # w8 for response like this? I dont like it...
        size = self.__dev.read(1)
        payload = self.__dev.read(toInt(size))  # and then the payload
        response = self.__exMessageHandler.handleMessage(payload)
        logger.error(f" UNIMPLEMENTED!!! after LuxRequest {response}")
        return 0

    def connect(self):
        return self.handshake()

    def disconnect(self):
        if self.__dev is not None:
            self.__dev.close()
            self.handshaked = False
