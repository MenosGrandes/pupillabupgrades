import numpy as np
from time import sleep
from .luxometer_device_if import LuxometerDeviceIf


class LuxometerDeviceDummyBase(LuxometerDeviceIf):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def readFromDevice(self):
        sleep(0.05)
        return self.value

    @property
    def online(self):
        return True

    @property
    def name(self):
        return f"{self.__class__.__name__}{self.value}"

    def connect(self):
        pass

    def disconnect(self):
        pass
    
    @property
    def connected(self):
        return True


class LuxometerDeviceDummySinus(LuxometerDeviceIf):
    def __init__(self):
        super().__init__()
        self.__sinGenerator = self.__getSinValue()

    def readFromDevice(self):
        sleep(0.05)
        return next(self.__sinGenerator) * 300

    def __getSinValue(self):
        s = np.pi
        while True:
            yield np.sin(s)
            s += 0.1

    @property
    def name(self):
        return "LuxometerDummySinus"

    def connect(self):
        pass

    def disconnect(self):
        pass

    @property
    def online(self):
        return True
    
    @property
    def connected(self):
        return True
