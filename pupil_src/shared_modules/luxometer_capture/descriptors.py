from dataclasses import dataclass


@dataclass
class descriptors:
    DESCRIPTORS_BASE: int = 0
    DESCRIPTOR_TSL2591: int = 9
    DESCRIPTOR_TSL2591X: int = 2
    DESCRIPTOR_GROVE_LIGHT_SENSOR_V1_1: int = 3
    DESCRIPTOR_GROVE_LIGHT_SENSOR_V1_0: int = 4
    DESCRIPTOR_TSL2561: int = 5
    DESCRIPTOR_VEML7700: int = 6
    DESCRIPTOR_GY49: int = 7


@dataclass
class commands:
    OK: int = 20299
    RETURN_OK: int = 19279
    READ_LUX: int = 28784
    SEND_DESCRIPTOR: int = 25715


@dataclass
class timing:
    DEFAULT_DELAY_MS: int = 500
