import pylibftdi as ftdi
import logging
from .luxometer_device_if import LuxometerDeviceIf

logger = logging.getLogger(__name__)


class LuxometerDeviceFTDI(LuxometerDeviceIf):
    def __init__(self):
        super().__init__()
        self.__dev = None

    
    def connected(self):
        return True

    @property
    def name(self):
        return f"{self.__class__.__name__}"

    @property
    def online(self):
        dev = None
        try:
            dev = ftdi.Device()
        except ftdi._base.FtdiError as e:
            logger.error("FtdiError. Turn on debug mode to see it")
            logger.debug(e)
            return False
        else:
            dev.close()
            return True

    def connect(self):
        self.__dev = ftdi.Device()
        self.__dev.baudrate = 9600
        if self.__dev is None:
            logger.error("FTDI device not connected")
        else:
            logger.error(f"FTDI device connected {self.__dev}")

    def readFromDevice(self):
        if self.__dev is None:
            logger.debug("MenosGrandes ftdi device is none")
            return 0
        if self.__dev._opened is False:
            logger.debug("Device not opened")
            return 0
        ret = -1
        #self.__dev.write(b"b") MENOSGRANDES TEMPORARY!
        logger.debug("MenosGrandes readFromDevice")
        #ret = int.from_bytes(self.__dev.read(2), "big")
        read_line = self.__dev.readline().strip()
        logger.debug(f"MenosGrandes readFromDevice = {read_line}")
        return 23

    def disconnect(self):
        if self.__dev:
            self.__dev.close()
