"""
(*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2019 Pupil Labs

Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""
import logging
from .luxometer_sources import (
    LuxometerSourceDummy100,
    LuxometerSourceDummy21,
    LuxometerSourceFTDI,
    LuxometerSourceTSL2591,
    LuxometerSourceDummySinus,
)
from .luxometer_backend import Luxometer_Manager

logger = logging.getLogger(__name__)
luxometer_source_classes = [
    LuxometerSourceFTDI,
    LuxometerSourceDummy100,
    LuxometerSourceDummy21,
    LuxometerSourceTSL2591,
    LuxometerSourceDummySinus,
]
luxometer_manager_classes = [Luxometer_Manager]
