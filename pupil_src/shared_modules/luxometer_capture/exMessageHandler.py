import os
from typing import Optional
import serial
from .exProtocol_pb2 import _EXMESSAGE, EXMessage, Valid
import logging

logger = logging.getLogger(__name__)


def toInt(byte):
    return int.from_bytes(byte, byteorder="little")


def fromInt(_int: int):
    return _int.to_bytes(1, "little")


class EXMessageHandler:
    def __init__(self):
        self.__possibleSignalsName = [i.name for i in _EXMESSAGE.fields]
        self.__dispatcher = {
            "setupRequest": self.__handleSetupRequest,
            "setupResponse": self.__handleSetupResponse,
            "fetchLuxRes": self.__handleFetchLuxResponse,
        }

    def __getMsgType(self, exMessage: EXMessage) -> Optional[str]:
        return exMessage.WhichOneof("payload")

    def __handleFetchLuxResponse(self, msg: EXMessage) -> float:
        """ """
        logger.debug(f"{msg}")
        return msg.fetchLuxRes.luxValue

    def __handleSetupRequest(self, msg: EXMessage) -> EXMessage:
        """
        Handle received ExSetupRequest and saves the descriptor for the device
        Succes:
            return ExSetupResponse
        Failed:
            log error
        """
        logger.debug(f"HandleSetupRequest got {msg}");
        setupResponse = EXMessage()
        setupResponse.setupResponse.valid = Valid.YES
        logger.debug(f"HandleSetupRequest returning {setupResponse}");
        return setupResponse

    def __handleLuxRequest(self, msg):
        return None

    def __handleSetupResponse(self, msg):
        return None

    def __handleError(self, msg):
        logger.debug(f"Unknown nanoPb EX signal handling, drop it {msg}")
        return None

    def handleMessage(self, msg: bytes):
        msgType = ""
        try:
            exMessage = EXMessage()
            exMessage.ParseFromString(msg)
            msgType = self.__getMsgType(exMessage)
        except Exception as e:
            logger.error(f"{e}")
        return self.__dispatcher.get(msgType, self.__handleError)(exMessage)
