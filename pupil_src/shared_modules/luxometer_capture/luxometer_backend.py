"""
(*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2019 Pupil Labs

Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""

import logging
from time import monotonic, sleep
from typing import NamedTuple
from uuid import UUID
import numpy as np
from pyglui import cygl

import gl_utils
from plugin import Plugin
from launchables.luxometer import device_process

from multiprocessing import (
    Process,
    Value,
    active_children,
    set_start_method,
    freeze_support,
)

# from abc import ABC, abstractmethod
# MenosGrandes TODO from .luxometer import Luxometer as Luxometer
from .luxometer_sources import (
    LuxometerSourceDummySinus,
    LuxometerSourceDummy21,
    LuxometerSourceDummy35,
    LuxometerSourceDummy100,
    LuxometerSourceFTDI,
    LuxometerSourceBase,
    LuxometerSourceTSL2591,
)

logger = logging.getLogger(__name__)


class InitialisationError(Exception):
    pass


class LuxometerDevice(NamedTuple):
    id: UUID
    name: str
    luxometer_source: LuxometerSourceBase


class Luxometer_Manager(Plugin):

    uniqueness = "by_class"
    gui_name = "Luxometer Manager"
    icon_chr = chr(0xEC01)
    icon_font = "pupil_icons"

    def __init__(self, g_pool):
        logger.debug("Initialized luxometer Manager")
        super().__init__(g_pool)

        self.port_name = "/dev/ttyUSB0"
        self.default_luxometer_source = LuxometerSourceDummy21(
            self.g_pool, self.port_name
        )
        self.luxometer_sources = [
            LuxometerSourceDummy35,
            LuxometerSourceDummy100,
            LuxometerSourceDummySinus,
            LuxometerSourceTSL2591,
        ]
        self.updateOnlineDevicesList()
        self.device = None
        self.activate_source(self.default_luxometer_source)
        self.spawnLuxometerProcess()

    def spawnLuxometerProcess(self):
        logger.debug("MenosGrandes spawnLuxometerProcess")
        Process(
            target=device_process.luxometer_device_process,
            name="luxometer_device_process",
            args=(
                self.g_pool.timebase,
                self.g_pool.ipc_pub_url,
                self.g_pool.ipc_sub_url,
                self.g_pool.ipc_push_url,
                self.g_pool.user_dir,
            ),
        ).start()

    def on_notify(self, notification):
        if (
            notification["subject"].startswith("luxometer.spawn_process")
            and notification["proc_name"] == self.g_pool.process
        ):
            self.spawnLuxometerProcess()

    def activate_source(self, luxometer_source):
        """
        MenosGrandes
        Tutaj startuje plugin od urzadzenia i cala komunikacja z urzadzeniem

        """

        self.notify_all(
            {
                "subject": "start_luxometer_plugin",
                "target": self.g_pool.process,
                "name": luxometer_source.__class__.__name__,
                "args": {"port": self.port_name},
            }
        )

    def deinit_ui(self):
        logger.debug("Deinit ui")
        self.remove_menu()

    def updateOnlineDevicesList(self):
        self.onlineDevices = []
        self.onlineDevices.append(
            LuxometerDevice(
                self.default_luxometer_source.id,
                self.default_luxometer_source.name,
                self.default_luxometer_source,
            )
        )
        for device in self.luxometer_sources:
            d = device(self.g_pool, self.port_name)
            if d.online is True:
                self.onlineDevices.append(LuxometerDevice(d.id, d.name, d))

    def get_online_devices(self):
        devices = [(name, source) for _, source, name in self.onlineDevices]
        return zip(*devices)

    def init_ui(self):
        logger.debug("initialized UI for Luxometer Manager")
        from pyglui import ui

        self.add_menu()
        self.menu.label = "Select Luxometer Source"
        updateButton = ui.Button("Update online devices", self.updateOnlineDevicesList)
        self.menu.append(updateButton)
        self.menu.append(
            ui.Selector(
                "selected_luxometer_source",
                selection_getter=self.get_online_devices,
                getter=lambda: self.device,
                setter=self.activate_source,
                label="LuxometerDevice",
            )
        )
        self.menu.append(
            ui.Text_Input(
                "port_name",
                self,
                setter=self.set_port,
                label="Port of serial device",
            )
        )

    def set_port(self, val):
        if not val:
            self.port_name = "/dev/ttyUSB0"
        else:
            self.port_name = val

    def add_menu(self):
        super().add_menu()
        from pyglui import ui

        self.menu_icon.order = 0.1

        self.menu.label = "Luxometer Manager"
