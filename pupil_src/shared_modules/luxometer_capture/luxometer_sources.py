"""
MenosGrandes TODO:
    __handleLuxometerFramEvent zawiera w sobie wartosci lux i wysyla je na pupil_socket. reaguje na 'luxometer_frame', ktore jest wysylane z funkcji recent_events w luxometer_sources.py klasie LuxometerSourceBase. Klasa ta jest podstawa dla kazdego LuxometerSource.
Kazdy luxometer source ma w sobie obiekt odpowiadajacy za komunikacje z luxometrem( czy to fizycznie z socketem, czy to zestubowany).

Wiec.. kadze wywolanie recent_event powoduje komunikacje z luxometrem. Jezeli cos sie zatnie albo bedzie trzeba czekac to wtedy caly proces sie zatrzymuje i czeka na opdowiedz z urzadzenia
"""
"""
(*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2019 Pupil Labs

Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""

import logging

import gl_utils
from plugin import Plugin
from abc import ABC, abstractmethod
from .luxometer_device_ftdi import LuxometerDeviceFTDI
from .luxometer_device_dummy import LuxometerDeviceDummyBase, LuxometerDeviceDummySinus
from .luxometer_TSL2591 import LuxometerDeviceSerial

logger = logging.getLogger(__name__)


class InitialisationError(Exception):
    pass


class LuxometerSourceBase(Plugin, ABC):

    uniqueness = "by_base_class"

    def __init__(self, g_pool, port):
        super().__init__(g_pool)
        self.luxometer = None
        self.g_pool.luxometer_capture = self
        self.port = port

    def on_close(self):
        self.luxometer.disconnect()

    @property
    def name(self):
        return self.luxometer.name

    def get_init_dict(self):
        return {}

    @property
    def online(self):
        return self.luxometer.online

    @property
    def connected(self):
        return True

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def disconnect(self):
        pass

    @property
    def id(self):
        return self.luxometer.id

    def cleanup(self):
        logger.debug(f"cleanup in {self.__class__.__name__}")
        self.luxometer.disconnect()


class LuxometerSourceFTDI(LuxometerSourceBase):
    def __init__(self, g_pool, port):
        super().__init__(g_pool, port)
        self.luxometer = LuxometerDeviceFTDI()

    def connect(self):
        logger.debug(f"Connecting to : {self.__class__.__name__}")
        self.luxometer.connect()

    def disconnect(self):
        self.luxometer.disconnect()

    @property
    def connected(self):
        return True


class LuxometerSourceTSL2591(LuxometerSourceBase):
    def __init__(self, g_pool, port):
        super().__init__(g_pool, port)
        self.luxometer = LuxometerDeviceSerial(self.port)

    def connect(self):
        logger.debug(f"Connetiong to : {self.__class__.__name__}")
        self.luxometer.connect()

    def disconnect(self):
        self.luxometer.disconnect()

    @property
    def connected(self):
        return self.luxometer.connected


class LuxometerSourceDummySinus(LuxometerSourceBase):
    def __init__(self, g_pool, port):
        super().__init__(g_pool, port)
        self.luxometer = LuxometerDeviceDummySinus()

    def connect(self):
        logger.error(f"Connecting to : {self.__class__.__name__}")
        pass

    def disconnect(self):
        pass


class LuxometerSourceDummy35(LuxometerSourceBase):
    def __init__(self, g_pool, port):
        super().__init__(g_pool, port)
        self.luxometer = LuxometerDeviceDummyBase(35)

    def connect(self):
        logger.debug(f"Connecting to : {self.__class__.__name__}")
        pass

    def disconnect(self):
        pass


class LuxometerSourceDummy100(LuxometerSourceBase):
    def __init__(self, g_pool, port):
        super().__init__(g_pool, port)
        self.luxometer = LuxometerDeviceDummyBase(100)

    def connect(self):
        logger.debug(f"Connecting to : {self.__class__.__name__}")
        pass

    def disconnect(self):
        pass


class LuxometerSourceDummy21(LuxometerSourceBase):
    def __init__(self, g_pool, port):
        super().__init__(g_pool, port )
        self.luxometer = LuxometerDeviceDummyBase(21)

    def connect(self):
        logger.debug(f"Connecting to : {self.__class__.__name__}")
        pass

    def disconnect(self):
        pass


class LuxometerSourceGroup:
    """Create a group of Luxometer Sources that can work together:
    ie. spawn 3 individual LuxometerSourceTSL2591 and get values from them
    """

    def __init__(self):
        pass
