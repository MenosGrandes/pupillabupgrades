
## """
## (*)~---------------------------------------------------------------------------
## Pupil - eye tracking platform
## Copyright (C) 2012-2019 Pupil Labs
##
## Distributed under the terms of the GNU
## Lesser General Public License (LGPL v3.0).
## See COPYING and COPYING.LESSER for license details.
## ---------------------------------------------------------------------------~(*)
## """
## # logging
## import logging
## import sched
## import time
## from collections import namedtuple
##
## import numpy as np
## import OpenGL.GL as gl
## import usb.core
## from pyglui import ui
## from pyglui.cygl.utils import RGBA, draw_points_norm, draw_polyline_norm
## from scipy.spatial import ConvexHull
##
## from calibration_routines.calibrate import closest_matches_monocular
## from glfw import *
## from plugin import Plugin
##
## logger = logging.getLogger(__name__)
##
## # MG UNUSED!!!!
##
##
## class LuxometerReader(Plugin):
##     order = 0.8
##     icon_chr = chr(0xEC12)
##     icon_font = "pupil_icons"
## #
##
##     def __init__(
##             self,
##             g_pool,
##     ):
##         super().__init__(g_pool)
##         self.usb_device = None
##         self.connect()
## #
##
##     def init_ui(self):
##         self.add_menu()
##         self.menu.label = "Luxometer"
## #
##
##         def ignore(_):
##             pass
## #
##
##     def deinit_ui(self):
##         self.remove_menu()
## #
##
##     def connect(self):
##         self.usb_device = usb.core.find(idVendor=0x0403, idProduct=0x6001)
##         if self.usb_device is None:
##             logger.error("Luxometer not found!")
##         else:
##             logger.info("Luxometer found!")
## #
##         reattach = False
##         if self.usb_device.is_kernel_driver_active(0):
##             reattach = True
##             self.usb_device.detach_kernel_driver(0)
## #
##         self.usb_device.set_configuration()
##         cfg = self.usb_device.get_active_configuration()
## #
##         interface_number = cfg[(0, 0)].bInterfaceNumber
##         alternate_settting = usb.control.get_interface(
##             self.usb_device, interface_number)
##         intf = usb.util.find_descriptor(cfg, bInterfaceNumber=interface_number,
##                                         bAlternateSetting=alternate_settting)
## #
##         ep = usb.util.find_descriptor(intf, custom_match=lambda e:
##                                       usb.util.endpoint_direction(e.bEndpointAddress) ==
##                                       usb.util.ENDPOINT_OUT)
##         lux = self.readFromDevice()
##         logger.error('Lux = {}'.format(lux))
##         """
##        switch to onRemove? Or something like that function
##        """
##         # This is needed to release interface, otherwise attach_kernel_driver fails
##         # due to "Resource busy"
##         usb.util.dispose_resources(self.usb_device)
## #
##         # It may raise USBError if there's e.g. no kernel driver loaded at all
##         if reattach:
##             self.usb_device.attach_kernel_driver(0)
## #
##
##     def readFromDevice(self):
##         msg = b'b'
##         self.usb_device.write(0x02, msg, 100)
##         data = self.usb_device.read(0x81, 100)
##         return data[0] * 256 + data[1]
