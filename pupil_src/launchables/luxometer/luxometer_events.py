from enum import Enum


class LuxometerEvent(Enum):
    LUXOMETER_FRAME = str("luxometer_frame")
