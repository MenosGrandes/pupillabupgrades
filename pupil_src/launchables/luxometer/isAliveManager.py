import time


class Is_Alive_Manager(object):
    """
    A context manager to wrap the is_alive flag.
    Is alive will stay true as long is the luxometer process is running.
    """

    def __init__(self, is_alive, ipc_socket, logger):
        self.is_alive = is_alive
        self.ipc_socket = ipc_socket
        self.logger = logger

    def __enter__(self):
        self.is_alive.value = True
        self.ipc_socket.notify({"subject": "luxometer_process.started"})
        return self

    def __exit__(self, etype, value, traceback):
        if etype is not None:
            import traceback as tb

            self.logger.error(
                "Process  crashed with trace:\n"
                + "".join(tb.format_exception(etype, value, traceback))
            )

        self.is_alive.value = False
        self.ipc_socket.notify({"subject": "luxometer_process.stopped"})
        time.sleep(1.0)
        return True  # do not propergate exception

