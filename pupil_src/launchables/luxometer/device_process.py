from types import SimpleNamespace
import zmq
from .luxometer_events import LuxometerEvent
import zmq_tools
import traceback
import signal
import time
from . import luxometer_handlers as lh
from shared_modules.luxometer_capture.luxometer_sources import *

"""
luxometer_device_process is another process which directly communicates with Luxometer_Source ( in this class is called device)



"""


def luxometer_device_process(
    timebase,
    ipc_pub_url,
    ipc_sub_url,
    ipc_push_url,
    user_dir,
    overwrite_cap_settings=None,
    hide_ui=False,
    debug=False,
    pub_socket_hwm=None,
    parent_application="capture",
):
    def interrupt_handler(sig, frame):
        trace = traceback.format_stack(f=frame)
        logger.debug(f"Caught signal {sig} in:\n" + "".join(trace))
        # NOTE: Interrupt is handled in world/service/player which are responsible for
        # shutting down the eye process properly

    g_pool = SimpleNamespace()

    # make some constants avaiable
    g_pool.debug = debug
    g_pool.user_dir = user_dir
    g_pool.app = parent_application
    g_pool.process = "[luxometer_device_process]"
    g_pool.timebase = timebase

    signal.signal(signal.SIGINT, interrupt_handler)
    zmq_ctx = zmq.Context()
    ipc_socket = zmq_tools.Msg_Dispatcher(zmq_ctx, ipc_push_url)
    luxometer_socket = zmq_tools.Msg_Streamer(zmq_ctx, ipc_pub_url, pub_socket_hwm)
    luxometer_device_sub = zmq_tools.Msg_Receiver(
        zmq_ctx, ipc_sub_url, topics=("luxometer_device_process",)
    )
    notify_sub = zmq_tools.Msg_Receiver(
        zmq_ctx,
        ipc_sub_url,
        topics=("notify",),
    )

    g_pool.zmq_ctx = zmq_ctx
    g_pool.ipc_pub = ipc_socket
    g_pool.ipc_pub_url = ipc_pub_url
    g_pool.ipc_sub_url = ipc_sub_url
    g_pool.ipc_push_url = ipc_push_url

    import logging
    from uvc import get_time_monotonic

    def get_timestamp():
        return get_time_monotonic() - g_pool.timebase.value

    g_pool.get_timestamp = get_timestamp
    g_pool.get_now = get_time_monotonic

    logging.getLogger("OpenGL").setLevel(logging.ERROR)
    logger = logging.getLogger()
    logger.handlers = []
    logger.setLevel(logging.NOTSET)
    logger.addHandler(zmq_tools.ZMQ_handler(zmq_ctx, ipc_push_url))
    # create logger for the context of this function
    logger = logging.getLogger(__name__)

    device = None  # Is should be stated as Luxometer_Source class, not device itself

    notificationHandler = lh.LuxometerNotificationHandler(g_pool, luxometer_socket)
    eventHandler = lh.LuxometerEventHandler(
        g_pool, g_pool.get_timestamp(), luxometer_socket
    )
    shouldStop = False
    start_time = g_pool.get_timestamp()
    t = 0
    while not shouldStop:
        t = g_pool.get_timestamp() - start_time
        if device is not None:
            if device.online:
                if not device.connected:
                    device.connect()
                else:
                    luxValue = device.luxometer.readFromDevice()
                    logger.debug(f"ReadFromDevice {luxValue}")
                    eventHandler.handleEvent(
                        {
                            "subject": LuxometerEvent.LUXOMETER_FRAME.value,
                            "lux": luxValue,
                            "timestamp": g_pool.get_timestamp(),
                        }
                    )

        # MenosGrandes do I need this sub? TODO
        if luxometer_device_sub.new_data:
            t, notification = luxometer_device_sub.recv()
            try:
                logger.debug(f"device_process luxometer_device_sub.recv!")
                notificationHandler.handleNotification(notification)
            except KeyError as e:
                logger.debug(f"luxometer_device_sub KeyError e = {e}")
            except lh.CloseLuxometerProcess:
                logger.error("MenosGrandes kill device_proces")
                break

        if notify_sub.new_data:
            t, notification = notify_sub.recv()
            logger.debug(f"MenosGrandes notigy_sub {t}")
            logger.debug(f"MenosGrandes notift_sub {notification}")

            subject = None
            try:
                subject = notification["subject"]
            except KeyError as e:
                logger.debug(f"No subject in {notification}, \n Error is {e}")
            if subject == "start_luxometer_plugin":
                # MenosGrandes why is it like this???
                device_name = notification["name"]
                logger.error(f"Startuje {device_name}")
                device = globals()[device_name](
                    g_pool, notification["args"]["port"]
                )  # Weird way to spawn object from string name.
                shouldStop = False

            # logger.debug("Przed")

    logger.debug("device_proccess shutting down")
    device.disconnect()
