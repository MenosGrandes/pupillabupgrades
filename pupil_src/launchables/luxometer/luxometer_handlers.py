"""
(*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2020 Pupil Labs

Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""
from .luxometer_notifications import LuxometerNotification
from .luxometer_events import LuxometerEvent
import logging

logger = logging.getLogger()
logger.handlers = []
logger.setLevel(logging.NOTSET)
# create logger for the context of this function
logger = logging.getLogger(__name__)


class CloseLuxometerProcess(Exception):
    pass


class LuxometerEventHandler(object):
    def __init__(self, g_pool, ts, pupil_socket):
        self.ts = ts  # MG Create some kind of handler for grapgs...
        self.g_pool = g_pool
        # I know i do not have graphs in gpool
        self.pupil_socket = pupil_socket
        self.__dispatcher = {
            LuxometerEvent.LUXOMETER_FRAME.value: self.__handleLuxometerFrameEvent
        }
        self.__currentEvent = ""

    def __handleError(self, event):
        # MG show that this is the error somehow
        logger.error(f" {self.g_pool.process} : Unhandled event {event}")
        pass

    def handleEvent(self, event):
        if not event:
            return

        subject = ""
        try:
            subject = event["subject"]
        except KeyError:
            self.__handleError(event)

        if self.__currentEvent is not subject:
            logger.error(f"{self.g_pool.process} :Handle new  Event = {event}")
            self.__currentEvent = subject

        self.__dispatcher.get(subject, self.__handleError)(event)

    def __handleLuxometerFrameEvent(self, event):
        # MG what should be done here?
        lux = event["lux"]
        t = event["timestamp"]
        self.pupil_socket.send(  # MenosGrandes to jest notifikacja!
            {
                "topic": LuxometerNotification.LUXOMETER_FRAME.value,
                "subject": "lux", #MenosGrandes do I need that?
                "luxValue": lux,
                "luxTimestamp": t,
            }
        )


class LuxometerNotificationHandler(object):
    def __init__(self, g_pool, luxometer_device_process_socket):
        self.g_pool = g_pool
        self.luxometer_device_process_socket = luxometer_device_process_socket
        self.__dispatcher = {
            LuxometerNotification.START_LUXOMETER_PLUGIN.value: self.__handleStartLuxometerPlugin,
            LuxometerNotification.STOP_LUXOMETER.value: self.__handleStopLuxometer,
            LuxometerNotification.START_LUXOMETER.value: self.__handleStartLuxometer,
            LuxometerNotification.STOP_LUXOMETER_PLUGIN.value: self.__handleStopLuxometerPlugin,
            LuxometerNotification.STOP_LUXOMETER_DEVICE_PROCESS.value: self.__handleStopLuxometerDeviceProcess,
            LuxometerNotification.LUXOMETER_FRAME.value: self.__handleLuxValue,
        }

    def __handleLuxValue(self, *args, **kwargs):
        logger.debug(f"{self.g_pool.process} :Co ja mam z tym teraz ztobic?")

    def __handleStopLuxometerPlugin(self, plugin, *args, **kwargs):
        try:
            plugin_to_stop = self.g_pool.plugin_by_name[plugin["name"]]
        except KeyError as err:
            logger.debug(
                f"{self.g_pool.process} :Attempt to load unknown plugin: {err}"
            )
        else:
            plugin_to_stop.alive = False
            self.g_pool.luxometer_plugins.clean()

    def __handleStopLuxometer(self, *args, **kwargs):
        raise CloseLuxometerProcess

    def __handleStartLuxometerPlugin(self, plugin, *args, **kwargs):
        logger.debug(f"{self.g_pool.process} : START of {plugin}")
        try:
            self.g_pool.luxometer_plugins.add(
                self.g_pool.plugin_by_name[plugin["name"]], plugin.get("args", {})
            ),
        except KeyError as e:
            logger.debug(f"{self.g_pool.process} :Attemp to load unknown plugin : {e}")

    def __handleStartLuxometer(self, *args, **kwargs):
        pass

    def __handleError(self, notification):
        logger.debug(
            f"{self.g_pool.process} :Error handling unknown notification = {notification}"
        )

    def __handleStopLuxometerDeviceProcess(self, *args, **kwargs):
        raise CloseLuxometerProcess

    def handleNotification(self, notification):
        subject = None
        try:
            subject = notification["subject"]
        except:
            logger.debug(f"{self.g_pool.process} :No subject in {notification}")
            return

        logger.debug(f"{self.g_pool.process} :Handle notification {subject} ")
        self.__dispatcher.get(subject, self.__handleError)(notification)
        for plugin in self.g_pool.luxometer_plugins:
            plugin.on_notify(notification)
