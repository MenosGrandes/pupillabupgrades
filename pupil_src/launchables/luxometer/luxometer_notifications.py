from enum import Enum


class LuxometerNotification(Enum):
    STOP_LUXOMETER = str("luxometer_process.should_stop")
    START_LUXOMETER_PLUGIN = str("start_luxometer_plugin")
    START_LUXOMETER = str("luxometer_process.started")
    STOP_LUXOMETER_PLUGIN = str("stop_luxometer_plugin")
    STOP_LUXOMETER_DEVICE_PROCESS = str("stop_luxometer_device_process")
    LUXOMETER_FRAME = str("frame.luxometer")
