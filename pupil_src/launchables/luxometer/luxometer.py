"""
(*)~---------------------------------------------------------------------------
Pupil - eye tracking platform
Copyright (C) 2012-2020 Pupil Labs

Distributed under the terms of the GNU
Lesser General Public License (LGPL v3.0).
See COPYING and COPYING.LESSER for license details.
---------------------------------------------------------------------------~(*)
"""


"""
luxometer_socket reaguje na sygnaly z luxometer_device_process z topikiem lux


LuxometerNotificationHandler odbiera sygnaly


eventHandler ma w sobie pupil_socket na ktory powinny isc wszystkie message

"""

from multiprocessing import (
    Process,
    Value,
    active_children,
    set_start_method,
    freeze_support,
)
import os
import platform
import signal
import time

from types import SimpleNamespace

from . import luxometer_handlers as lh
from . import luxometer_notifications as lux_notification
from . import isAliveManager
from . import device_process

# from pupil_src.launchables import luxometer


def luxometer_process(
    timebase,
    is_alive_flag,
    ipc_pub_url,
    ipc_sub_url,
    ipc_push_url,
    user_dir,
    version,
    overwrite_cap_settings=None,
    hide_ui=False,
    debug=False,
    pub_socket_hwm=None,
    parent_application="capture",
):

    import zmq
    import zmq_tools

    zmq_ctx = zmq.Context()
    ipc_socket = zmq_tools.Msg_Dispatcher(zmq_ctx, ipc_push_url)
    pupil_socket = zmq_tools.Msg_Streamer(zmq_ctx, ipc_pub_url, pub_socket_hwm)
    # Socket on which all request will be send!
    luxometer_socket = zmq_tools.Msg_Streamer(zmq_ctx, ipc_pub_url, None)
    luxometer_device_sub = zmq_tools.Msg_Receiver(
        zmq_ctx, ipc_sub_url, topics=("luxometer_frame",)
    )
    notify_sub = zmq_tools.Msg_Receiver(
        zmq_ctx,
        ipc_sub_url,
        topics=("notify",),
    )

    # logging setup
    import logging

    logging.getLogger("OpenGL").setLevel(logging.ERROR)
    logger = logging.getLogger()
    logger.handlers = []
    logger.setLevel(logging.NOTSET)
    logger.addHandler(zmq_tools.ZMQ_handler(zmq_ctx, ipc_push_url))
    # create logger for the context of this function
    logger = logging.getLogger(__name__)

    if is_alive_flag.value:
        # indicates eye process that this is a duplicated startup
        return

    with isAliveManager.Is_Alive_Manager(is_alive_flag, ipc_socket, logger):
        # general imports
        import traceback
        import numpy as np

        from OpenGL.GL import GL_COLOR_BUFFER_BIT

        # display
        import glfw
        from gl_utils import GLFWErrorReporting

        GLFWErrorReporting.set_default()

        from pyglui import ui, graph, cygl
        from pyglui.cygl.utils import draw_points, RGBA, draw_polyline
        from pyglui.cygl.utils import Named_Texture
        import gl_utils
        from gl_utils import basic_gl_setup, adjust_gl_view, clear_gl_screen
        from gl_utils import make_coord_system_pixel_based
        from gl_utils import make_coord_system_norm_based
        from gl_utils import is_window_visible, glViewport

        # monitoring
        import psutil

        # Plug-ins
        from plugin import Plugin_List

        # helpers/utils
        from uvc import get_time_monotonic
        from file_methods import Persistent_Dict
        from version_utils import parse_version
        from methods import normalize, denormalize, timer
        from luxometer_capture import (
            luxometer_source_classes,
            luxometer_manager_classes,
        )
        from roi import Roi

        from background_helper import IPC_Logging_Task_Proxy

        IPC_Logging_Task_Proxy.push_url = ipc_push_url

        def interrupt_handler(sig, frame):
            trace = traceback.format_stack(f=frame)
            logger.debug(f"Caught signal {sig} in:\n" + "".join(trace))
            # NOTE: Interrupt is handled in world/service/player which are responsible for
            # shutting down the eye process properly

        signal.signal(signal.SIGINT, interrupt_handler)

        # UI Platform tweaks
        if platform.system() == "Linux":
            scroll_factor = 10.0
            window_position_default = (600, 300 + 30)
        elif platform.system() == "Windows":
            scroll_factor = 10.0
            window_position_default = (600, 90 + 300)
        else:
            scroll_factor = 1.0
            window_position_default = (600, 300)

        icon_bar_width = 50
        window_size = None
        content_scale = 1.0

        # g_pool holds variables for this process
        g_pool = SimpleNamespace()

        # make some constants avaiable
        g_pool.debug = debug
        g_pool.user_dir = user_dir
        g_pool.version = version
        g_pool.app = parent_application
        g_pool.process = "[luxometer_proces]"
        g_pool.timebase = timebase

        g_pool.zmq_ctx = zmq_ctx
        g_pool.ipc_pub = ipc_socket
        g_pool.ipc_pub_url = ipc_pub_url
        g_pool.ipc_sub_url = ipc_sub_url
        g_pool.ipc_push_url = ipc_push_url

        def get_timestamp():
            return get_time_monotonic() - g_pool.timebase.value

        g_pool.get_timestamp = get_timestamp
        g_pool.get_now = get_time_monotonic

        luxometer_plugins = luxometer_manager_classes + luxometer_source_classes
        g_pool.luxometer_capture_manager = luxometer_manager_classes[0]
        g_pool.plugin_by_name = {p.__name__: p for p in luxometer_plugins}

        default_luxometer_manager_name = "Luxometer_Manager"
        default_luxometer_capture_settings = {}
        # Only Manager is loaded
        default_luxometer_plugins = [
            (default_luxometer_manager_name, default_luxometer_capture_settings)
        ]

        def consume_events_and_render_buffer():
            glfw.make_context_current(main_window)
            clear_gl_screen()

            if all(
                c > 0 for c in g_pool.camera_render_size
            ):  # WTF is that? MenosGrandes
                glViewport(0, 0, *g_pool.camera_render_size)
                for p in g_pool.luxometer_plugins:
                    p.gl_display()

            glViewport(0, 0, *window_size)
            # render graphs
            [g.draw() for g in g_pool.graphs.values()]

            # render GUI
            try:
                clipboard = glfw.get_clipboard_string(main_window).decode()
            except (AttributeError, glfw.GLFWError):
                # clipboard is None, might happen on startup
                clipboard = ""
            g_pool.gui.update_clipboard(clipboard)
            user_input = g_pool.gui.update()
            if user_input.clipboard != clipboard:
                # only write to clipboard if content changed
                glfw.set_clipboard_string(main_window, user_input.clipboard)

            for button, action, mods in user_input.buttons:
                x, y = glfw.get_cursor_pos(main_window)
                pos = gl_utils.window_coordinate_to_framebuffer_coordinate(
                    main_window, x, y, cached_scale=None
                )
                pos = normalize(pos, g_pool.camera_render_size)
                # Position in img pixels
                # pos = denormalize(pos, g_pool.luxometer_capture.frame_size) Mg

                for plugin in g_pool.luxometer_plugins:
                    if plugin.on_click(pos, button, action):
                        break

            for key, scancode, action, mods in user_input.keys:
                for plugin in g_pool.luxometer_plugins:
                    if plugin.on_key(key, scancode, action, mods):
                        break

            for char_ in user_input.chars:
                for plugin in g_pool.luxometer_plugins:
                    if plugin.on_char(char_):
                        break

            # update screen
            glfw.swap_buffers(main_window)

        # Callback functions
        def on_resize(window, w, h):
            nonlocal window_size
            nonlocal content_scale

            is_minimized = bool(glfw.get_window_attrib(window, glfw.ICONIFIED))

            if is_minimized:
                return

            # Always clear buffers on resize to make sure that there are no overlapping
            # artifacts from previous frames.
            gl_utils.glClear(GL_COLOR_BUFFER_BIT)
            gl_utils.glClearColor(0, 0, 0, 1)

            active_window = glfw.get_current_context()
            glfw.make_context_current(window)
            content_scale = gl_utils.get_content_scale(window)
            framebuffer_scale = gl_utils.get_framebuffer_scale(window)
            g_pool.gui.scale = content_scale
            window_size = w, h
            g_pool.camera_render_size = w - int(icon_bar_width * g_pool.gui.scale), h
            g_pool.gui.update_window(w, h)
            g_pool.gui.collect_menus()
            for g in g_pool.graphs.values():
                g.scale = content_scale
                g.adjust_window_size(w, h)
            adjust_gl_view(w, h)
            glfw.make_context_current(active_window)

            # Minimum window size required, otherwise parts of the UI can cause openGL
            # issues with permanent effects. Depends on the content scale, which can
            # potentially be dynamically modified, so we re-adjust the size limits every
            # time here.
            min_size = int(2 * icon_bar_width * g_pool.gui.scale / framebuffer_scale)
            glfw.set_window_size_limits(
                window,
                min_size,
                min_size,
                glfw.DONT_CARE,
                glfw.DONT_CARE,
            )

            # Needed, to update the window buffer while resizing
            consume_events_and_render_buffer()

        def on_window_key(window, key, scancode, action, mods):
            g_pool.gui.update_key(key, scancode, action, mods)

        def on_window_char(window, char):
            g_pool.gui.update_char(char)

        def on_iconify(window, iconified):
            g_pool.iconified = iconified

        def on_window_mouse_button(window, button, action, mods):
            g_pool.gui.update_button(button, action, mods)

        def on_pos(window, x, y):
            x, y = gl_utils.window_coordinate_to_framebuffer_coordinate(
                window, x, y, cached_scale=None
            )
            g_pool.gui.update_mouse(x, y)

            pos = x, y
            pos = normalize(pos, g_pool.camera_render_size)
            # Position in img pixels
            # pos = denormalize(pos, g_pool.luxometer_capture.frame_size) Mg!

            for p in g_pool.luxometer_plugins:
                p.on_pos(pos)

        def on_scroll(window, x, y):
            g_pool.gui.update_scroll(x, y * scroll_factor)

        def on_drop(window, paths):
            for plugin in g_pool.luxometer_plugins:
                if plugin.on_drop(paths):
                    break

        # load session persistent settings
        session_settings = Persistent_Dict(
            os.path.join(g_pool.user_dir, "user_settings_luxometer")
        )
        if parse_version(session_settings.get("version", "0.0")) != g_pool.version:
            logger.info(
                "Session setting are from a different version of this app. I will not use those."
            )
            session_settings.clear()

        g_pool.iconified = False
        g_pool.luxometer_capture = None

        # Initialize glfw
        glfw.init()
        glfw.window_hint(glfw.SCALE_TO_MONITOR, glfw.TRUE)
        if hide_ui:
            glfw.window_hint(glfw.VISIBLE, 0)  # hide window
        title = "Luxometer"
        default_window_size = 500 + icon_bar_width, 500
        width, height = session_settings.get("window_size", default_window_size)

        main_window = glfw.create_window(width, height, title, None, None)

        window_position_manager = gl_utils.WindowPositionManager()
        window_pos = window_position_manager.new_window_position(
            window=main_window,
            default_position=window_position_default,
            previous_position=session_settings.get("window_position", None),
        )
        glfw.set_window_pos(main_window, window_pos[0], window_pos[1])

        glfw.make_context_current(main_window)
        cygl.utils.init()

        # gl_state settings
        basic_gl_setup()
        # Color of backgroud in main window of this process
        g_pool.image_tex = Named_Texture()
        g_pool.image_tex.update_from_ndarray(np.ones((1, 1), dtype=np.uint8) + 125)

        # setup GUI
        g_pool.gui = ui.UI()
        g_pool.menubar = ui.Scrolling_Menu(
            "Settings", pos=(-500, 0), size=(-icon_bar_width, 0), header_pos="left"
        )
        g_pool.iconbar = ui.Scrolling_Menu(
            "Icons", pos=(-icon_bar_width, 0), size=(0, 0), header_pos="hidden"
        )
        g_pool.gui.append(g_pool.menubar)
        g_pool.gui.append(g_pool.iconbar)

        """
        Write proper settings to file if possible MENOSGRANDES TODO!
        """
        # luxometer_plugins_to_load = session_settings.get(
        #    "loaded_luxometer_plugins", default_luxometer_plugins
        # )

        luxometer_plugins_to_load = default_luxometer_plugins

        if overwrite_cap_settings:
            # Ensure that overwrite_cap_settings takes preference over source plugins
            # with incorrect settings that were loaded from session settings.
            luxometer_plugins_to_load.append(overwrite_cap_settings)

        # Add runtime plugins to the list of plugins to load with default arguments,
        # if not already restored from session settings
        luxometer_plugins_to_load_names = set(
            name for name, _ in luxometer_plugins_to_load
        )

        g_pool.luxometer_plugins = Plugin_List(g_pool, luxometer_plugins_to_load)

        if not g_pool.luxometer_capture:
            # Make sure we always have a capture running. Important if there was no
            # capture stored in session settings.
            g_pool.luxometer_plugins.add(
                g_pool.plugin_by_name[default_luxometer_manager_name],
                default_luxometer_capture_settings,
            )

        # toggle_general_settings(True)

        g_pool.rec_path = None

        # Register callbacks main_window
        glfw.set_framebuffer_size_callback(main_window, on_resize)
        glfw.set_window_iconify_callback(main_window, on_iconify)
        glfw.set_key_callback(main_window, on_window_key)
        glfw.set_char_callback(main_window, on_window_char)
        glfw.set_mouse_button_callback(main_window, on_window_mouse_button)
        glfw.set_cursor_pos_callback(main_window, on_pos)
        glfw.set_scroll_callback(main_window, on_scroll)
        glfw.set_drop_callback(main_window, on_drop)

        # load last gui configuration
        g_pool.gui.configuration = session_settings.get("ui_config", {})
        # If previously selected plugin was not loaded this time, we will have an
        # expanded menubar without any menu selected. We need to ensure the menubar is
        # collapsed in this case.
        if all(submenu.collapsed for submenu in g_pool.menubar.elements):
            g_pool.menubar.collapsed = True

        def updateLuxGraph():
            return 100;
        # set up performance graphs
        def setupGraphs():
            pid = os.getpid()
            ps = psutil.Process(pid)
            ts = g_pool.get_timestamp()

            cpu_graph = graph.Bar_Graph()
            cpu_graph.pos = (20, 50)
            cpu_graph.update_fn = ps.cpu_percent
            cpu_graph.update_rate = 5
            cpu_graph.label = "CPU %0.1f"

            lux_graph = graph.Bar_Graph(data_points=100, min_val=0, max_val=900)
            lux_graph.pos = (30, 300)
            lux_graph.update_rate = 1
            lux_graph.label = "%0.0f Lux"
            lux_graph.scale = 30
            lux_graph.bar_width = 4

            fps_graph = graph.Bar_Graph()
            fps_graph.pos = (140, 50)
            fps_graph.update_rate = 1
            fps_graph.label = "%0.0f FPS"
            g_pool.graphs = {"cpu": cpu_graph, "fps": fps_graph, "lux": lux_graph}
            return ts, cpu_graph, fps_graph, lux_graph

        ts, cpu_graph, fps_graph, lux_graph = setupGraphs()
        g_pool.graphs = {"cpu": cpu_graph, "fps": fps_graph, "lux": lux_graph}
        # set the last saved window size
        on_resize(main_window, *glfw.get_framebuffer_size(main_window))

        # create a timer to control window update frequency
        window_update_timer = timer(1 / 60)

        def window_should_update():
            return next(window_update_timer)

        logger.warning("Process started.")

        if platform.system() == "Darwin":
            # On macOS, calls to glfw.swap_buffers() deliberately take longer in case of
            # occluded windows, based on the swap interval value. This causes an FPS drop
            # and leads to problems when recording. To side-step this behaviour, the swap
            # interval is set to zero.
            #
            # Read more about window occlusion on macOS here:
            # https://developer.apple.com/library/archive/documentation/Performance/Conceptual/power_efficiency_guidelines_osx/WorkWhenVisible.html
            glfw.swap_interval(0)

        # Event loop
        notificationHandler = lh.LuxometerNotificationHandler(g_pool, luxometer_socket)
        eventHandler = lh.LuxometerEventHandler(g_pool, ts, pupil_socket)

        def updateWindow():
            if window_should_update():
                cpu_graph.update()
                if is_window_visible(main_window):
                    consume_events_and_render_buffer()
                glfw.poll_events()
                return glfw.window_should_close(main_window)


        while not updateWindow():
            event = {}
            if luxometer_device_sub.new_data:
                t, notification = luxometer_device_sub.recv()
                logger.debug(f"Luxometer device sub received in {self.g_pool.process}{t}")
                try:
                    notificationHandler.handleNotification(notification)
                except KeyError as e:
                    logger.error(f"KeyError e = {e}")
                except lh.CloseLuxometerProcess:
                    break

            if notify_sub.new_data:
                t, notification = notify_sub.recv()
                try:
                    notificationHandler.handleNotification(notification)
                except KeyError as e:
                    logger.error(f"KeyError e = {e}")
                except lh.CloseLuxometerProcess:
                    break

            for plugin in g_pool.luxometer_plugins:
                plugin.recent_events(event)

            eventHandler.handleEvent(event)

        session_settings[
            "loaded_luxometer_plugins"
        ] = g_pool.luxometer_plugins.get_initializers()
        # save session persistent settings
        session_settings["ui_config"] = g_pool.gui.configuration
        session_settings["version"] = str(g_pool.version)

        if not hide_ui:
            glfw.restore_window(main_window)  # need to do this for windows os
            session_settings["window_position"] = glfw.get_window_pos(main_window)
            session_window_size = glfw.get_window_size(main_window)
            if 0 not in session_window_size:
                f_width, f_height = session_window_size
                if platform.system() in ("Windows", "Linux"):
                    # Store unscaled window size as the operating system will scale the
                    # windows appropriately during launch on Windows and Linux.
                    f_width, f_height = (
                        f_width / content_scale,
                        f_height / content_scale,
                    )
                session_settings["window_size"] = int(f_width), int(f_height)

        session_settings.close()
        # kill luxometer_device_process
        luxometer_socket.send(
            {
                "topic": "luxometer_device_process",
                "subject": lux_notification.LuxometerNotification.STOP_LUXOMETER_DEVICE_PROCESS.value,
            }
        )
        logger.debug("killing luxometer device_process")
        for plugin in g_pool.luxometer_plugins:
            plugin.alive = False
        g_pool.luxometer_plugins.clean()

    glfw.destroy_window(main_window)
    g_pool.gui.terminate()
    glfw.terminate()
    logger.info("Luxometer process shutting down.")


# def eye_profiled(
#    timebase,
#    is_alive_flag,
#    ipc_pub_url,
#    ipc_sub_url,
#    ipc_push_url,
#    user_dir,
#    version,
#    eye_id,
#    overwrite_cap_settings=None,
#    hide_ui=False,
#    debug=False,
#    pub_socket_hwm=None,
#    parent_application="capture",
# ):
#    import cProfile
#    import subprocess
#    import os
#    from .eye import eye
#
#    cProfile.runctx(
#        (
#            "eye("
#            "timebase, "
#            "is_alive_flag, "
#            "ipc_pub_url, "
#            "ipc_sub_url, "
#            "ipc_push_url, "
#            "user_dir, "
#            "version, "
#            "eye_id, "
#            "overwrite_cap_settings, "
#            "hide_ui, "
#            "debug, "
#            "pub_socket_hwm, "
#            "parent_application, "
#            ")"
#        ),
#        {
#            "timebase": timebase,
#            "is_alive_flag": is_alive_flag,
#            "ipc_pub_url": ipc_pub_url,
#            "ipc_sub_url": ipc_sub_url,
#            "ipc_push_url": ipc_push_url,
#            "user_dir": user_dir,
#            "version": version,
#            "eye_id": eye_id,
#            "overwrite_cap_settings": overwrite_cap_settings,
#            "hide_ui": hide_ui,
#            "debug": debug,
#            "pub_socket_hwm": pub_socket_hwm,
#            "parent_application": parent_application,
#        },
#        locals(),
#        "eye{}.pstats".format(eye_id),
#    )
#    loc = os.path.abspath(__file__).rsplit("pupil_src", 1)
#    gprof2dot_loc = os.path.join(loc[0], "pupil_src", "shared_modules", "gprof2dot.py")
#    subprocess.call(
#        "python "
#        + gprof2dot_loc
#        + " -f pstats eye{0}.pstats | dot -Tpng -o eye{0}_cpu_time.png".format(eye_id),
#        shell=True,
#    )
#    print(
#        "created cpu time graph for eye{} process. Please check out the png next to the eye.py file".format(
#            eye_id
#        )
#    )
