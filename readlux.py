import serial
import time
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys
import pylibftdi as ftdi

'''
p = ftdi.Device()
d.baudrate = 38400


'''
ARDUINO = False;
dev = 0
print(sys.argv)
serial_port = '/dev/ttyUSB0'
print(serial_port)
if sys.argv[1] == 'FTDI':
    print("Using libFTDI")
    dev = ftdi.Device()
    dev.baudrate = 38400
    print(f"{dev}")
elif sys.argv[1] == 'SERIAL':
    print("Using Serial")
    if len(sys.argv) > 2 :
        if sys.argv[2] != '':
            serial_port = sys.argv[2]
            ARDUINO = True
            print("ARDUINO")
    dev = serial.Serial(serial_port, 38400, timeout=0.05)
else:
    print("Command line argument missing. Must by FTDI or SERIAL")
    exit(1)


def getlux():
    if ARDUINO:
        dev.write('r')
        f = 0
        dupa = False
        while dupa is False:
            try:
                data = dev.readline()
                print(data)
                f = float(data)
            except ValueError:
                pass
            else:
                dupa = True
        return f
    else:
        b = bytearray(2)
        dev.write(b'b')
        if sys.argv[1] == 'SERIAL':
            dev.readinto(b)
            return b[0]*256 + b[1]
        elif sys.argv[1] == 'FTDI':
            ret = (int.from_bytes(dev.read(2), "big"))
            print(ret)
            return ret
            print(b)


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = []
ys = []

def animate(i, xs, ys):
    lux = getlux()

    # Add x and y to lists
    xs.append(dt.datetime.now().strftime('%f'))
    ys.append(lux)

    # Limit x and y lists to 20 items
    xs = xs[-20:]
    ys = ys[-20:]
    # Draw x and y lists
    ax.clear()
    ax.plot(xs, ys)

    # Format plot
    plt.xticks(rotation=45, ha='right')
    plt.subplots_adjust(bottom=0.30)
    plt.title('Light intensity over time')
    plt.ylabel('Ligt intensity [lux]')


# Set up plot to call animate() function periodically

ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys), interval=1)

plt.show()

dev.close()
